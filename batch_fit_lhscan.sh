#!/bin/zsh

#SBATCH -o /work/ws/atlas/cm465-Fitter/run/logs/lhscan_%A_%a.log
#SBATCH -p nemo_vm_atlhei
#SBATCH -t 2:00:00
#SBATCH --mem=1000
#SBATCH --array=0-3,5-20

config=ssww_Mjj_LHScan_${SLURM_ARRAY_TASK_ID}.config

# trex-fitter h config/LHScan/Asimov/${config}
trex-fitter d config/LHScan/${config}
# trex-fitter w config/LHScan/Asimov/${config}
trex-fitter f config/LHScan/${config}
trex-fitter p config/LHScan/${config}
