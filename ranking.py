import numpy as np
import os

config = "ssww_DNN"

syst_names = []
with open("config/%s.config"%config) as f:
    for line in f:
        if "Systematic:" in line and "%" not in line:
            syst_names.append(line.split(" ")[1].strip('""').strip())


syst_names = [i.strip().strip('"') for i in syst_names]
syst_names.append("mu_WZ")
print(syst_names)

for i in syst_names:
    submit_job = "trex-fitter r config/%s.config Ranking=%s"%(config,i)
    options = [
        "-o /work/ws/atlas/cm465-Fitter/run/logs/ranking.%s.log"%i,
        "-p nemo_vm_atlhei",
        "--get-user-env",
        "-t 48:00:00",
        "--mem=2000",
        "--wrap \"{}\"".format(submit_job),
        "--output=/dev/null",
        "--error=/dev/null"
    ]
    os.system("sbatch "+" ".join(options))
